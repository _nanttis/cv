import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const functions = {
  introMaps: {
    experience (data) {
      return (
        data.map((data, index) => {
          return (
            <Row key={index} className='small-space'>
              <Col className='small-text' xs={5}>
                <Row>
                  <Col>
                    <strong>{data.company}</strong>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {data.time}
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {data.location}
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row>
                  <Col>
                    <strong>{data.title}</strong>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {data.companyDesc} - {this.state.descExtended ? data.descExtended : data.desc}
                  </Col>
                </Row>
              </Col>
            </Row>
          )
        })
      )
    },
    education (data) {
      return (
        data.map((data, index) => {
          return (
            <Row key={index} className='small-space'>
              <Col className='small-text' xs={5}>
                <Row>
                  <Col>
                    <strong>{data.desc}</strong>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {data.time}
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {data.location}
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row>
                  <Col>
                    <strong>{data.title}</strong>
                  </Col>
                </Row>
              </Col>
            </Row>
          )
        })
      )
    },
    expertise (data, path) {
      return (
        data.map((data, index) => {
          return (
            <img src={path + data.src} className='cv-image' alt='' key={index} />
          )
        })
      )
    },
    social (data, path) {
      return (
        data.map((data, index) => {
          return (
            <Row key={index}>
              <Col xs={2}>
                <img src={path + data.src} className='cv-image social' alt='' />
              </Col>
              <Col className='vertical-align'>
                {data.link}
              </Col>
            </Row>
          )
        })
      )
    },
    references (data) {
      return (
        data.map((data, index) => {
          return (
            <Col key={index} className='small-space align'>
              <Row>
                <Col>
                  <strong>{data.name}</strong>
                </Col>
              </Row>
              <Row>
                <Col>
                  {data.title}
                </Col>
              </Row>
              <Row>
                <Col>
                  {data.company}
                </Col>
              </Row>
              <Row>
                <Col className='small-text'>
                  {data.email}
                </Col>
              </Row>
              <Row>
                <Col className='small-text'>
                  {data.phone}
                </Col>
              </Row>
            </Col>
          )
        })
      )
    },
    interests (data) {
      return (
        data.map((data, index) => {
          return (
            <Row key={index}>
              <Col>
                <ul><li>{data}</li></ul>
              </Col>
            </Row>
          )
        })
      )
    }
  },
  renderEdges () {
    const div = []
    const width = 50

    const getStylePosition = [
      () => {
        return {
          left: width,
          top: width
        }
      },
      () => {
        return {
          right: width,
          top: width
        }
      },
      () => {
        return {
          right: width,
          bottom: width
        }
      },
      () => {
        return {
          left: width,
          bottom: width
        }
      }
    ]

    for (let i = 0; i < 4; i++) {
      const deg = i * 90

      const styleRotation = {
        position: 'absolute',
        transform: `rotate(${deg}deg)`
      }

      const stylePosition = getStylePosition[i]()

      const style = { ...styleRotation, ...stylePosition }

      div.push(<div key={i} className='edge-triangle' style={style} />)
    }

    return div
  },
  handleShowExtendedDesc () {
    this.setState(state => (
      {
        descExtended: !state.descExtended
      }
    ))
  }
}

export default functions
