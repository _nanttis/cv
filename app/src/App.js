import React from 'react'
import functions from './functions'
import data from './data.json'
import './styles.scss'
import { Container, Row, Col, OverlayTrigger, Button, Popover, Image } from 'react-bootstrap'

class App extends React.Component {
  constructor () {
    super()
    this.intro = this.intro.bind(this)
    this.sidebar = this.sidebar.bind(this)
    this.controls = this.controls.bind(this)
    this.renderEdges = functions.renderEdges.bind(this)
    this.experience = functions.introMaps.experience.bind(this)
    this.education = functions.introMaps.education.bind(this)
    this.expertise = functions.introMaps.expertise.bind(this)
    this.social = functions.introMaps.social.bind(this)
    this.references = functions.introMaps.references.bind(this)
    this.interests = functions.introMaps.interests.bind(this)

    this.handleShowExtendedDesc = functions.handleShowExtendedDesc.bind(this)

    this.state = {
      descExtended: false
    }
  }

  intro () {
    return (
      <Container>
        <Row>
          <Col className='large-space' />
        </Row>

        <Row>
          <Col className='align uppercase '><h4>{data.info.name}</h4></Col>
        </Row>

        <Row>
          <Col className='align large-space'><h6 className='gray'>{data.info.title}</h6></Col>
        </Row>

        <Row>
          <Col className='align'>{data.info.address}</Col>
        </Row>

        <Row className='large-space align'>
          <Col><strong>T:</strong> {data.info.tel}</Col>
          <Col><strong>E:</strong> {data.info.email}</Col>
          <Col><strong>W:</strong> {data.info.web}</Col>
        </Row>

        <Row>
          <Col className='align'>
            <strong className='header'>Profile</strong>
          </Col>
        </Row>
        <Row className='large-space'>
          <Col className='align'>
            {data.info.profile}
          </Col>
        </Row>

        <Row>
          <Col className='large-space' />
        </Row>

        <Row>
          <Col>

            <Row>
              <Col>
                <strong className='header'>Experience</strong>
              </Col>
            </Row>
            <Row className='large-space'>
              <Col>
                {this.experience(data.experience)}
              </Col>
            </Row>

            {this.state.descExtended ? '' : this.educationSection()}

          </Col>

          {this.state.descExtended ? '' : this.expertiseSection()}

        </Row>

        <Row>
          <Col className='large-space' />
        </Row>

        {this.state.descExtended ? '' : this.referencesSection()}

      </Container>
    )
  }

  educationSection () {
    return (
      <>
        <Row>
          <Col>
            <strong className='header'>Education</strong>
          </Col>
        </Row>
        <Row className='large-space'>
          <Col>
            {this.education(data.education)}
          </Col>
        </Row>
      </>
    )
  }

  referencesSection () {
    return (
      <Row>
        <Col>
          <Row>
            <Col className='align'>
              <strong className='header'>References</strong>
            </Col>
          </Row>
          <Row className='large-space'>
            {this.references(data.references)}
          </Row>
        </Col>
      </Row>
    )
  }

  expertiseSection () {
    return (
      <>
        <Col className='divider' xs={1} />
        <Col>
          <Row>
            <Col>
              <strong className='header'>Expertise</strong>
            </Col>
          </Row>
          <Row className='large-space'>
            <Col>
              {this.expertise(data.expertise, data.settings.imagePath)}
            </Col>
          </Row>
          <Row className='large-space'>
            <Col>
              <Row>
                <Col>
                  <strong className='header'>Social</strong>
                </Col>
              </Row>
              {this.social(data.social, data.settings.imagePath)}
            </Col>
            <Col xs={4}>
              <Row>
                <Col>
                  <strong className='header'>Interests</strong>
                </Col>
              </Row>
              {this.interests(data.interests)}
            </Col>
          </Row>
        </Col>
      </>
    )
  }

  sidebar () {
    const popover = (
      <Popover id='popover-basic'>
        <Popover.Title as='h3'>Source</Popover.Title>
        <Popover.Content>
          <Row>
            <Col>
              <Image src={data.settings.imagePath + 'bitbucket.png'} className='regular-image' />
            </Col>
            <Col className='vertical-align'>
              <a href='https://bitbucket.org/_nanttis/cv'>bitbucket.org/_nanttis/cv</a>
            </Col>
          </Row>

        </Popover.Content>
      </Popover>
    )

    return (
      <OverlayTrigger trigger='click' placement='left' overlay={popover}>
        <Button variant='outline-dark' id='source'>
          <Image src={data.settings.imagePath + 'source.svg'} fluid />
        </Button>
      </OverlayTrigger>
    )
  }

  extend () {
    return (
      <Button
        variant='outline-dark'
        id='extend'
        onClick={this.handleShowExtendedDesc}
      >
        <Image src={data.settings.imagePath + 'info.svg'} fluid />
      </Button>
    )
  }

  controls () {
    return (
      <div id='controls'>
        {this.sidebar()}
        {this.extend()}
      </div>
    )
  }

  render () {
    return (
      <div className='page'>
        {this.renderEdges()}
        {this.intro()}
        {this.controls()}
      </div>
    )
  }
}

export default App
